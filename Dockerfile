FROM python:3.7
RUN apt-get update && apt-get install -y nginx

RUN chmod -R 777 /var/log/nginx/
RUN chmod -R 777 /var/lib/nginx/
RUN mkdir /nginx && chmod -R 777 /nginx
RUN rm /etc/nginx/sites-enabled/default

# config app
RUN mkdir /cluster_gateway/
ADD ./requirements /cluster_gateway/requirements
WORKDIR /cluster_gateway

# Install app dependencies
RUN pip3 install -r requirements

ADD . .

# run nginx
RUN ./manage.py collectstatic --no-input
COPY ./nginx/nginx.conf /etc/nginx/nginx.conf
COPY nginx/cluster_gateway.conf /etc/nginx/sites-enabled/

RUN mkdir -p /var/user_data
RUN chmod 600 /var/user_data -R
RUN chown 1000:1000 /var/user_data -R

# Run the app
ENTRYPOINT ["./docker-entrypoint.sh"]

CMD ["SERVE"]

# nginx port
EXPOSE 7000

USER 1000:1000
