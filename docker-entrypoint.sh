#!/bin/bash

case "$1" in
  "SERVE")
    nginx & gunicorn --workers $WORKER_COUNT --bind 0.0.0.0:8000 ClusterGateway.wsgi:application
    ;;
  "MIGRATE")
    ./manage.py migrate --no-input
    ;;
  *)
    exec "$@"
    ;;
esac
