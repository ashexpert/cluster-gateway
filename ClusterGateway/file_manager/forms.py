from django import forms


class UploadFileForm(forms.Form):
    directory = forms.CharField(widget=forms.HiddenInput())
    file = forms.FileField()
    is_zip = forms.BooleanField(required=False)

# from django import forms
# from django.forms import ClearableFileInput
# from .models import UploadPdf
# class ResumeUpload(forms.ModelForm):
#     class Meta:
#         model = UploadPdf
#         fields = [‘resumes’]
#         widgets = {
#             ‘resumes’: ClearableFileInput(attrs={‘multiple’: True}),
#         }