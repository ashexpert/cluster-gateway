from django.urls import path

from ClusterGateway.file_manager.views import get_file, get_path, upload_file


urlpatterns = [
    path('files/', get_path),
    path('download/', get_file),
    path('upload/', upload_file)
]
