import logging

from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_GET, require_POST
from django.http.response import HttpResponseBadRequest, FileResponse, HttpResponseRedirect

from ClusterGateway.file_manager.forms import UploadFileForm
from ClusterGateway.file_manager.filesystem import get_rel_path, get_base_name, get_parent_dir


logger = logging.getLogger(__name__)


@require_GET
@login_required(login_url="/login/")
def get_path(req):
    user = req.user
    rel_dir = req.GET.get('path') or "/"
    directories, files = user.get_dir_content(rel_dir)
    named_directories = []
    named_files = []
    for name in directories:
        named_directories.append({"path": get_rel_path(name, user.base_directory),
                                  "name": get_base_name(name)})
    for name in files:
        named_files.append({"path": get_rel_path(name, user.base_directory),
                            "name": get_base_name(name)})

    return render(req, 'file_table.html',
                  {
                      "files": named_files,
                      "directories": named_directories,
                      "form": UploadFileForm(initial={'directory': rel_dir}),
                      "back_path": get_parent_dir(rel_dir)
                  })


@require_GET
@login_required(login_url="/login/")
def get_file(req):
    user = req.user
    rel_dir = req.GET.get('path')
    if rel_dir is None:
        return HttpResponseBadRequest("no path in get request")
    file = user.get_file(rel_dir)
    if file is None:
        return HttpResponseBadRequest("no such a file or directory")
    return FileResponse(file)


@require_POST
@login_required(login_url="/login/")
def upload_file(req):
    form = UploadFileForm(req.POST, req.FILES)
    if not form.is_valid():
        return HttpResponseBadRequest('invalid payload')
    is_zip = form.cleaned_data['is_zip']
    directory = form.cleaned_data['directory']
    for _, file in req.FILES.items():
        if not req.user.upload_file(file, file.name, directory, is_zip):
            logger.warning(f"could not save file {file.name}")
    return HttpResponseRedirect(req.headers.get('referer') or '/files')
