import os
import zipfile
import logging
from os import path

from django.conf import settings

logger = logging.getLogger("filesystem")

BASE_PATH = settings.BASE_PATH
if BASE_PATH is None:
    logger.warning("BASE_PATH is None you must set BASE_PATH environment variable")


def escape_path(rel_path):
    return path.abspath(path.join('/', rel_path))


def get_real_path(user_path, user_base_path):
    user_path = escape_path(user_path)
    real_path = path.join(BASE_PATH, user_base_path.strip('/'), user_path.strip('/'))
    return real_path


def get_rel_path(real_path, user_base_path):
    rel_path = path.relpath(real_path, path.join(BASE_PATH, user_base_path))
    return escape_path(rel_path)


def get_path_type(real_path):
    if not path.exists(real_path):
        return None

    return "D" if path.isdir(real_path) else "F"


def list_path(real_path):
    if not path.isdir(real_path):
        return []

    return list(path.join(real_path, name) for name in os.listdir(real_path))


def get_file(real_path):
    if not path.isfile(real_path):
        return None

    return open(real_path, 'rb')


def save_file(uploaded_file, name, real_path):
    try:
        with open(path.join(real_path, name), 'wb+') as dest:
            for chunk in uploaded_file.chunks():
                dest.write(chunk)
    except Exception as e:
        logger.error(f"can not save file: {e}")
        return False
    else:
        return True


def save_and_extract_zip(uploaded_file, name, real_path):
    save_file(uploaded_file, name, real_path)
    try:
        with zipfile.ZipFile(path.join(real_path, name), 'r') as source:
            source.extractall(real_path)
    except Exception as e:
        logger.error(f"can not extract zip file: {e}")
        return False
    else:
        return True


def get_base_name(file_path):
    return path.basename(file_path)


def get_parent_dir(directory):
    return os.path.abspath(os.path.join(directory, os.pardir))


def create_user_directory(user_path):
    real_path = get_real_path("/", user_path)
    if get_path_type(real_path) is None:
        os.mkdir(real_path, 0o766)
        return True
    else:  # user directory already exist
        logger.warning("user directory already exist")
        return False
