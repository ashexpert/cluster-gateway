from django import forms
from django.contrib.auth.forms import UserCreationForm

from ClusterGateway.user_auth.models import User


class UserAdminCreationForm(UserCreationForm):
    balance = forms.IntegerField(label='Balance', min_value=0)
    base_directory = forms.CharField(label='Base Directory')

    class Meta:
        model = User
        fields = '__all__'

    def save(self, commit=True):
        user = super().save(commit=False)
        user.balance = self.cleaned_data["balance"]
        user.base_directory = self.cleaned_data["base_directory"]

        if commit:
            user.save()
        return user
