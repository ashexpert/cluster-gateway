import logging

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required


logger = logging.getLogger(__name__)


def login_user(req):
    if req.method == "GET":
        return render(req, "login.html")
    elif req.method == "POST":
        username = req.POST.get('username')
        password = req.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is None:  # unknown account
            logger.info(f"user not found: {username}")
            return render(req, "login.html")
        elif not user.is_active:  # disabled user
            return render(req, "login.html")
        else:
            login(req, user)
            return HttpResponseRedirect("/panel/")


@login_required(login_url="/login/")
def panel(req):
    return render(req, "base.html")


def redirect_to_panel(_):
    return HttpResponseRedirect("/panel/")
