from django.urls import path

from ClusterGateway.user_auth.views import login_user, panel, redirect_to_panel


urlpatterns = [
    path('login/', login_user),
    path('panel/', panel),
    path('', redirect_to_panel)
]
