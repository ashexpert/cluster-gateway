from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from ClusterGateway.user_auth.models import User
from ClusterGateway.user_auth.forms import UserCreationForm


class ClusterUserAdmin(UserAdmin):
    add_form = UserCreationForm
    add_fieldsets = UserAdmin.add_fieldsets + (
        (None, {'fields': ('balance', 'base_directory')}),
    )


admin.site.register(User, ClusterUserAdmin)
