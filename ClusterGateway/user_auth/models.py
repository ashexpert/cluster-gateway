from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager

from ClusterGateway.file_manager import filesystem


class User(AbstractUser):
    balance = models.BigIntegerField(default=0)
    base_directory = models.CharField(max_length=50)

    objects = UserManager()

    def get_balance(self):
        return int(self.balance)

    def save(self, *args, **kwargs):
        filesystem.create_user_directory(self.base_directory)
        super().save(*args, **kwargs)

    def upload_file(self, uploaded_file, name, rel_dir, zipfile=False):
        real_dir = filesystem.get_real_path(rel_dir, str(self.base_directory))  # TODO is str redundant?
        if zipfile:
            return filesystem.save_and_extract_zip(uploaded_file, name, real_dir)
        else:
            return filesystem.save_file(uploaded_file, name, real_dir)

    def get_dir_content(self, rel_dir):
        real_dir = filesystem.get_real_path(rel_dir, str(self.base_directory))
        directories = []
        files = []
        for name in filesystem.list_path(real_dir):
            if filesystem.get_path_type(name) == "D":
                directories.append(name)
            else:
                files.append(name)

        return directories, files

    def get_file(self, rel_dir):
        real_dir = filesystem.get_real_path(rel_dir, str(self.base_directory))
        return filesystem.get_file(real_dir)

    def __str__(self):
        return str(self.username)
