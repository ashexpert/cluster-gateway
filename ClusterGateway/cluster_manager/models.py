import shlex
import random

from django.db import models

from ClusterGateway.user_auth.models import User
from ClusterGateway.file_manager.filesystem import get_real_path
from ClusterGateway.cluster_manager.nomad_controller import register_cpu_job, retrieve_job_allocations, get_alloc_logs


class Image(models.Model):
    name = models.CharField(max_length=100, db_index=True)  # name of image with version
    common_name = models.CharField(max_length=50)  # name that we show in panel
    enabled = models.BooleanField(null=False, blank=False, db_index=True)

    def __str__(self):
        return self.name


class Task(models.Model):
    class TaskStatus:
        CREATED = "CR"  # when task is created but not submitted
        WAITING = "WT"  # when task is submitted and waiting to get started
        RUNNING = "RN"  # when task is running on cluster
        ERROR = "ER"  # when task got into an error
        DONE = "DN"  # when task finished successfully
        REMOVED = "RM"  # task is soft deleted
        all_fields = ((CREATED, "Created"),
                      (WAITING, "Waiting"),
                      (REMOVED, "Removed"),
                      (RUNNING, "Running"),
                      (ERROR, "Error"),
                      (DONE, "Done"))

    user = models.ForeignKey(to=User, on_delete=models.CASCADE, db_index=True)
    job_id = models.CharField(max_length=50, unique=True)
    status = models.CharField(max_length=2, choices=TaskStatus.all_fields, default=TaskStatus.CREATED
                              , db_index=True)
    image_name = models.ForeignKey(to=Image, on_delete=models.DO_NOTHING, db_index=True)
    command = models.TextField()  # TODO do the user need to specify this field?
    args = models.TextField()
    env_vars = models.TextField()
    status_message = models.TextField()
    requested_cpu = models.PositiveIntegerField(null=False, blank=False)
    requested_ram = models.PositiveIntegerField(null=False, blank=False)

    # TODO GPU

    @staticmethod
    def _create_job_id():
        return "".join((random.choice("1234567890") for _ in range(30)))

    def submit_job(self):
        random_id = self._create_job_id()
        env_vars = list(map(lambda s: s.split('='), self.env_vars.split()))
        created = register_cpu_job(job_id=random_id,
                                   group_name=self.user.username + random_id,
                                   task_name=self.user.username + random_id,
                                   image_string=self.image_name.name,
                                   cmd=self.command,
                                   args=shlex.split(self.args),
                                   environment_variables=env_vars,
                                   request_cpu=self.requested_cpu,
                                   request_memory=self.requested_ram,
                                   user_data_dir=get_real_path("/", self.user.base_directory))
        if created:
            self.status_message = "created"
            self.job_id = random_id
            return True
        else:
            return False

    def save(self, *args, **kwargs):
        if self.pk is not None:
            return super().save()  # TOF naaabbi

        submitted = self.submit_job()

        if submitted:
            super().save()
            return True
        else:
            return False

    def get_logs(self):
        job_allocs = retrieve_job_allocations(self.job_id)
        if len(job_allocs) == 0:
            return {"stdout": "", "stderr": ""}

        alloc_id = job_allocs[0]['ID']
        stdout, stderr = get_alloc_logs(alloc_id, self.get_task_name())
        return {"stdout": stdout, "stderr": stderr}

    def get_task_name(self):
        return str(self.user.username) + str(self.job_id)

    def __str__(self):
        return self.job_id
