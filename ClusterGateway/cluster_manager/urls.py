from django.urls import path

from ClusterGateway.cluster_manager.views import TasksList, TaskCreate, TaskDetail


urlpatterns = [
    path('tasks/', TasksList.as_view()),
    path('tasks/create/', TaskCreate.as_view()),
    path('tasks/<str:job_id>/', TaskDetail.as_view())
]
