# Generated by Django 3.0.3 on 2020-07-23 09:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cluster_manager', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='args',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='task',
            name='command',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='task',
            name='job_id',
            field=models.CharField(default='', max_length=50, unique=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='task',
            name='requested_cpu',
            field=models.PositiveIntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='task',
            name='requested_ram',
            field=models.PositiveIntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='task',
            name='status',
            field=models.CharField(choices=[('CR', 'Created'), ('WT', 'Waiting'), ('RN', 'Running'), ('ER', 'Error'), ('DN', 'Done'), ('RM', 'Removed')], db_index=True, default='CR', max_length=2),
        ),
    ]
