from django.contrib import admin

from ClusterGateway.cluster_manager.models import Task, Image
# Register your models here.

admin.site.register(Task)
admin.site.register(Image)
