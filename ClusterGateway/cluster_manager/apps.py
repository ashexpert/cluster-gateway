from django.apps import AppConfig


class ClusterManagerConfig(AppConfig):
    name = 'cluster_manager'
