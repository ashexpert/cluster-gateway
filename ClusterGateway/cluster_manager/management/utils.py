import logging
from enum import Enum

from ClusterGateway.cluster_manager.nomad_controller import retrieve_job_allocations
from ClusterGateway.cluster_manager.models import Task


# TODO fix logging
logger = logging.getLogger()
logger.setLevel("INFO")


class JOB_STATUS(Enum):
    PENDING = 'P'
    RUNNING = 'R'
    FAILED = 'F'
    NOT_FOUND = 'D'
    COMPLETE = 'C'
    UNKNOWN = 'U'


client_status_to_job_status_map = {
    'failed': JOB_STATUS.FAILED,
    'pending': JOB_STATUS.PENDING,
    'running': JOB_STATUS.RUNNING,
    'complete': JOB_STATUS.COMPLETE,
}

job_status_to_task_status_map = {
    JOB_STATUS.RUNNING: Task.TaskStatus.RUNNING,
    JOB_STATUS.PENDING: Task.TaskStatus.WAITING,
    JOB_STATUS.FAILED: Task.TaskStatus.ERROR,
    JOB_STATUS.NOT_FOUND: Task.TaskStatus.REMOVED,
    JOB_STATUS.COMPLETE: Task.TaskStatus.DONE,
}


def get_job_status(job_id):
    allocs = retrieve_job_allocations(job_id)
    if len(allocs) == 0:
        logger.warning(f"job {job_id} has no allocation")
        return JOB_STATUS.NOT_FOUND
    elif len(allocs) > 1:
        logger.error(f"job {job_id} has more than 1 allocations")

    alloc = allocs[0]
    if alloc["ClientStatus"] in client_status_to_job_status_map.keys():
        return client_status_to_job_status_map[alloc["ClientStatus"]]
    else:
        logger.error(f"unknown alloc state {alloc['ClientStatus']}")
        return JOB_STATUS.UNKNOWN


def update_task(task):
    new_status = get_job_status(task.job_id)
    if new_status == JOB_STATUS.UNKNOWN:
        logger.warning(f"task {task.job_id} has unknown state")
        return

    if job_status_to_task_status_map[new_status] != task.status:
        logger.info(f"new status for task {task.job_id}: {new_status}")
        task.status = job_status_to_task_status_map[new_status]
        task.save()
