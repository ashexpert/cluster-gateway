import logging
from threading import Thread

from django.core.management.base import BaseCommand
from apscheduler.schedulers.blocking import BlockingScheduler

from ClusterGateway.cluster_manager.models import Task
from ClusterGateway.cluster_manager.management.utils import update_task


logger = logging.getLogger("main")


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('--checking-interval-seconds', type=int, default=30,
                            help="interval that this job will wakeup and check the cluster status")

        # parser.add_argument('cpu-job-price-per-core-second', type=int,
        #                     help="price of one second of one core usage(cpu job)")
        #
        # parser.add_argument('cpu-job-price-per-ram-second', type=int,
        #                     help="price of one second of one GB ram(cpu job)")
        #
        # parser.add_argument('gpu-job-price-per-core-second', type=int,
        #                     help="price of one second of one core usage(gpu job)")
        #
        # parser.add_argument('gpu-job-price-per-ram-second', type=int,
        #                     help="price of one second of one GB ram(gpu job)")
        #
        # parser.add_argument('gpu-price-per-second', type=int,
        #                     help="price of one GPU per second ram")

    @staticmethod
    def update_tasks():
        try:
            update_states = [Task.TaskStatus.RUNNING,
                             Task.TaskStatus.WAITING,
                             Task.TaskStatus.CREATED]
            for task in Task.objects.filter(status__in=update_states):
                Thread(target=update_task, args=(task,)).start()
        except Exception as e:
            logger.exception(f"exception occurred in updating task status: {e}")

    def handle(self, *args, **options):
        scheduler = BlockingScheduler()
        interval = options.get("checking_interval_seconds")
        scheduler.add_job(self.update_tasks, trigger='interval', seconds=interval)
        scheduler.start()
