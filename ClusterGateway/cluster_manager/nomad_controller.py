import nomad
from nomad.api.client import stream_logs
import jinja2
import logging
from django.conf import settings

client = nomad.Nomad(host=settings.NOMAD_CONFIG['host'])
streamer = stream_logs()
# client = nomad.Nomad(host="127.0.0.1")

template_loader = jinja2.FileSystemLoader(searchpath="./ClusterGateway/cluster_manager/job_templates/")
template_env = jinja2.Environment(loader=template_loader)


def register_cpu_job(job_id, group_name, task_name, image_string, cmd, args, environment_variables, request_cpu, request_memory, user_data_dir):
    job_spec = {
        'job_id': job_id,
        'group_name': group_name,
        'task_name': task_name,
        'image_name': image_string,
        'config': {
            'command': cmd,
            'args': str(args).replace("\'", "\""),
            'environment': environment_variables,
            'mount_location': user_data_dir
        },
        'spec': {
            'cpu': request_cpu * 1000,
            'memory': request_memory * 1024,
        },
    }
    try:
        cpu_template = template_env.get_template('cpu_template')
        hcl_out = cpu_template.render(job=job_spec)

        # parsing hcl to json
        json_out = client.jobs.parse(hcl_out, canonicalize=True)

        # registering the job
        client.job.register_job(job_id, {"Job": json_out})
    except Exception as e:
        logging.error("error in creating the job: {}".format(str(e)))
        return False
    else:
        return True


def retrieve_job(job_id):
    return client.job.get_job(job_id)


def retrieve_job_allocations(job_id):
    try:
        allocs = client.job.get_allocations(job_id)
    except Exception as e:
        logging.warning("exception at getting job allocations {}".format(str(e)))
        return []
    else:
        return allocs


def get_jobs_list(prefix=""):
    return client.jobs.get_jobs(prefix)


def get_alloc_logs(alloc_id, task_name):
    try:
        stdout = streamer.stream(alloc_id, task_name, "stdout")
        stderr = streamer.stream(alloc_id, task_name, "stderr")
    except Exception as e:
        logging.warning("exception at getting job allocations {}".format(str(e)))
        return "", ""
    else:
        return stdout, stderr

# print(stream_logs().stream(retrieve_job_allocations("267154728692744670749771694063")[0]['ID'], "admin267154728692744670749771694063", "stdout"))

# print("no")
# register_cpu_job("test","test","test","busybox",'sleep',["inf"],1024, 1024)
# register_cpu_job("test2","test","test2","busybox",'sleep',["inf"],1024, 1024)
# register_cpu_job("test3","test","test3","busybox",'sleep',["inf"],1024, 1024)
# print("done")
# print(retrieve_job("test"))
# print(retrieve_job_allocations("test"))
# print(get_jobs_list())


# retrieve_job output:
"""
{
  "JobModifyIndex": 215, 
  "Spreads": null, 
  "Namespace": "default", 
  "ModifyIndex": 215, 
  "Priority": 50, 
  "Version": 0, 
  "TaskGroups": [
    {
      "Count": 1, 
      "ShutdownDelay": null, 
      "Tasks": [
        {
          "Artifacts": null, 
          "Driver": "docker", 
          "KillTimeout": 5000000000, 
          "Env": null, 
          "Config": {
            "cpu_hard_limit": true, 
            "memory_hard_limit": 1024.0, 
            "args": "", 
            "command": "sleep inf", 
            "image": "busybox"
          }, 
          "Leader": false, 
          "Resources": {
            "Devices": null, 
            "Networks": null, 
            "MemoryMB": 1024, 
            "IOPS": 0, 
            "DiskMB": 0, 
            "CPU": 1024
          }, 
          "Templates": null, 
          "VolumeMounts": null, 
          "RestartPolicy": {
            "Delay": 15000000000, 
            "Attempts": 3, 
            "Interval": 86400000000000, 
            "Mode": "fail"
          }, 
          "User": "", 
          "Services": null, 
          "Lifecycle": null, 
          "Kind": "", 
          "Name": "test", 
          "DispatchPayload": null, 
          "CSIPluginConfig": null, 
          "KillSignal": "", 
          "Vault": null, 
          "Constraints": null, 
          "ShutdownDelay": 0, 
          "Meta": null, 
          "Affinities": null, 
          "LogConfig": {
            "MaxFiles": 10, 
            "MaxFileSizeMB": 10
          }
        }
      ], 
      "Name": "test", 
      "ReschedulePolicy": {
        "MaxDelay": 0, 
        "DelayFunction": "constant", 
        "Interval": 86400000000000, 
        "Unlimited": false, 
        "Delay": 5000000000, 
        "Attempts": 1
      }, 
      "EphemeralDisk": {
        "SizeMB": 300, 
        "Migrate": false, 
        "Sticky": false
      }, 
      "Migrate": null, 
      "Spreads": null, 
      "Update": null, 
      "Scaling": null, 
      "RestartPolicy": {
        "Delay": 15000000000, 
        "Attempts": 3, 
        "Interval": 86400000000000, 
        "Mode": "fail"
      }, 
      "Meta": null, 
      "Volumes": null, 
      "Services": null, 
      "Affinities": null, 
      "Networks": null, 
      "Constraints": null
    }
  ], 
  "Stable": false, 
  "CreateIndex": 215, 
  "Type": "batch", 
  "Status": "pending", 
  "AllAtOnce": false, 
  "Dispatched": false, 
  "Stop": false, 
  "Update": {
    "HealthyDeadline": 0, 
    "AutoRevert": false, 
    "Stagger": 0, 
    "Canary": 0, 
    "MaxParallel": 0, 
    "HealthCheck": "", 
    "AutoPromote": false, 
    "ProgressDeadline": 0, 
    "MinHealthyTime": 0
  }, 
  "StatusDescription": "", 
  "ParameterizedJob": null, 
  "Name": "test", 
  "Region": "global", 
  "Periodic": null, 
  "Datacenters": [
    "dc1"
  ], 
  "ID": "test", 
  "Constraints": null, 
  "VaultToken": "", 
  "ConsulToken": "", 
  "SubmitTime": 1594939878224512417, 
  "Meta": null, 
  "ParentID": "", 
  "Affinities": null, 
  "Payload": null
}
"""

# retrieve_job_allocations output
"""
[
  {
    "Namespace": "default", 
    "ModifyIndex": 232, 
    "FollowupEvalID": "", 
    "CreateIndex": 228, 
    "NodeName": "armin-laptop", 
    "TaskGroup": "test", 
    "ID": "a887c557-5568-d57b-7ff0-4ce1d2bc958f", 
    "DesiredStatus": "run", 
    "EvalID": "00dcfda3-2e4c-57b7-dd25-2945632ba38a", 
    "JobType": "batch", 
    "Name": "test.test[0]", 
    "DesiredTransition": {
      "Migrate": null, 
      "Reschedule": null, 
      "ForceReschedule": null
    }, 
    "PreemptedAllocations": null, 
    "ModifyTime": 1594939888807048652, 
    "JobID": "test", 
    "RescheduleTracker": {
      "Events": [
        {
          "Delay": 5000000000, 
          "PrevNodeID": "e33d0b6b-9378-8c18-8c7d-61d5508c84dd", 
          "PrevAllocID": "d8e2215b-fb3e-b36d-1d97-6e68074fc75a", 
          "RescheduleTime": 1594939883924489276
        }
      ]
    }, 
    "JobVersion": 0, 
    "PreemptedByAllocation": "", 
    "DeploymentStatus": null, 
    "NodeID": "e33d0b6b-9378-8c18-8c7d-61d5508c84dd", 
    "DesiredDescription": "", 
    "ClientDescription": "Failed tasks", 
    "ClientStatus": "failed", 
    "CreateTime": 1594939883924917237, 
    "TaskStates": {
      "test": {
        "LastRestart": null, 
        "Restarts": 0, 
        "Failed": true, 
        "State": "dead", 
        "FinishedAt": "2020-07-16T22:51:24.614344675Z", 
        "StartedAt": null, 
        "Events": [
          {
            "DisplayMessage": "Task received by client", 
            "TaskSignal": "", 
            "KillTimeout": 0, 
            "VaultError": "", 
            "Details": {}, 
            "TaskSignalReason": "", 
            "Type": "Received", 
            "DiskLimit": 0, 
            "DriverError": "", 
            "KillReason": "", 
            "FailsTask": false, 
            "FailedSibling": "", 
            "ValidationError": "", 
            "Signal": 0, 
            "KillError": "", 
            "DownloadError": "", 
            "Time": 1594939884105673479, 
            "RestartReason": "", 
            "DriverMessage": "", 
            "StartDelay": 0, 
            "GenericSource": "", 
            "SetupError": "", 
            "Message": "", 
            "ExitCode": 0
          }, 
          {
            "DisplayMessage": "Building Task Directory", 
            "TaskSignal": "", 
            "KillTimeout": 0, 
            "VaultError": "", 
            "Details": {
              "message": "Building Task Directory"
            }, 
            "TaskSignalReason": "", 
            "Type": "Task Setup", 
            "DiskLimit": 0, 
            "DriverError": "", 
            "KillReason": "", 
            "FailsTask": false, 
            "FailedSibling": "", 
            "ValidationError": "", 
            "Signal": 0, 
            "KillError": "", 
            "DownloadError": "", 
            "Time": 1594939884138191039, 
            "RestartReason": "", 
            "DriverMessage": "", 
            "StartDelay": 0, 
            "GenericSource": "", 
            "SetupError": "", 
            "Message": "Building Task Directory", 
            "ExitCode": 0
          }, 
          {
            "DisplayMessage": "3 errors occurred:\n\t* failed to parse config: \n\t* Invalid label: No argument or block type is named \"memory_hard_limit\".\n\t* Incorrect attribute value type: Inappropriate value for attribute \"args\": list of string required.\n\n", 
            "TaskSignal": "", 
            "KillTimeout": 0, 
            "VaultError": "", 
            "Details": {
              "validation_error": "3 errors occurred:\n\t* failed to parse config: \n\t* Invalid label: No argument or block type is named \"memory_hard_limit\".\n\t* Incorrect attribute value type: Inappropriate value for attribute \"args\": list of string required.\n\n"
            }, 
            "TaskSignalReason": "", 
            "Type": "Failed Validation", 
            "DiskLimit": 0, 
            "DriverError": "", 
            "KillReason": "", 
            "FailsTask": false, 
            "FailedSibling": "", 
            "ValidationError": "3 errors occurred:\n\t* failed to parse config: \n\t* Invalid label: No argument or block type is named \"memory_hard_limit\".\n\t* Incorrect attribute value type: Inappropriate value for attribute \"args\": list of string required.\n\n", 
            "Signal": 0, 
            "KillError": "", 
            "DownloadError": "", 
            "Time": 1594939884570409063, 
            "RestartReason": "", 
            "DriverMessage": "", 
            "StartDelay": 0, 
            "GenericSource": "", 
            "SetupError": "", 
            "Message": "", 
            "ExitCode": 0
          }, 
          {
            "DisplayMessage": "Error was unrecoverable", 
            "TaskSignal": "", 
            "KillTimeout": 0, 
            "VaultError": "", 
            "Details": {
              "fails_task": "true", 
              "restart_reason": "Error was unrecoverable"
            }, 
            "TaskSignalReason": "", 
            "Type": "Not Restarting", 
            "DiskLimit": 0, 
            "DriverError": "", 
            "KillReason": "", 
            "FailsTask": true, 
            "FailedSibling": "", 
            "ValidationError": "", 
            "Signal": 0, 
            "KillError": "", 
            "DownloadError": "", 
            "Time": 1594939884614337910, 
            "RestartReason": "Error was unrecoverable", 
            "DriverMessage": "", 
            "StartDelay": 0, 
            "GenericSource": "", 
            "SetupError": "", 
            "Message": "", 
            "ExitCode": 0
          }, 
          {
            "DisplayMessage": "Sent interrupt. Waiting 5s before force killing", 
            "TaskSignal": "", 
            "KillTimeout": 5000000000, 
            "VaultError": "", 
            "Details": {
              "kill_timeout": "5s"
            }, 
            "TaskSignalReason": "", 
            "Type": "Killing", 
            "DiskLimit": 0, 
            "DriverError": "", 
            "KillReason": "", 
            "FailsTask": false, 
            "FailedSibling": "", 
            "ValidationError": "", 
            "Signal": 0, 
            "KillError": "", 
            "DownloadError": "", 
            "Time": 1594939884682500173, 
            "RestartReason": "", 
            "DriverMessage": "", 
            "StartDelay": 0, 
            "GenericSource": "", 
            "SetupError": "", 
            "Message": "", 
            "ExitCode": 0
          }
        ]
      }
    }
  }, 
  {
    "Namespace": "default", 
    "ModifyIndex": 228, 
    "FollowupEvalID": "00dcfda3-2e4c-57b7-dd25-2945632ba38a", 
    "CreateIndex": 217, 
    "NodeName": "armin-laptop", 
    "TaskGroup": "test", 
    "ID": "d8e2215b-fb3e-b36d-1d97-6e68074fc75a", 
    "DesiredStatus": "stop", 
    "EvalID": "dae8c288-3cc0-f99f-7233-750ef0ccfa15", 
    "JobType": "batch", 
    "Name": "test.test[0]", 
    "DesiredTransition": {
      "Migrate": null, 
      "Reschedule": null, 
      "ForceReschedule": null
    }, 
    "PreemptedAllocations": null, 
    "ModifyTime": 1594939883924917237, 
    "JobID": "test", 
    "RescheduleTracker": null, 
    "JobVersion": 0, 
    "PreemptedByAllocation": "", 
    "DeploymentStatus": null, 
    "NodeID": "e33d0b6b-9378-8c18-8c7d-61d5508c84dd", 
    "DesiredDescription": "alloc was rescheduled because it failed", 
    "ClientDescription": "Failed tasks", 
    "ClientStatus": "failed", 
    "CreateTime": 1594939878326592475, 
    "TaskStates": {
      "test": {
        "LastRestart": null, 
        "Restarts": 0, 
        "Failed": true, 
        "State": "dead", 
        "FinishedAt": "2020-07-16T22:51:18.92399006Z", 
        "StartedAt": null, 
        "Events": [
          {
            "DisplayMessage": "Task received by client", 
            "TaskSignal": "", 
            "KillTimeout": 0, 
            "VaultError": "", 
            "Details": {}, 
            "TaskSignalReason": "", 
            "Type": "Received", 
            "DiskLimit": 0, 
            "DriverError": "", 
            "KillReason": "", 
            "FailsTask": false, 
            "FailedSibling": "", 
            "ValidationError": "", 
            "Signal": 0, 
            "KillError": "", 
            "DownloadError": "", 
            "Time": 1594939878404011052, 
            "RestartReason": "", 
            "DriverMessage": "", 
            "StartDelay": 0, 
            "GenericSource": "", 
            "SetupError": "", 
            "Message": "", 
            "ExitCode": 0
          }, 
          {
            "DisplayMessage": "Building Task Directory", 
            "TaskSignal": "", 
            "KillTimeout": 0, 
            "VaultError": "", 
            "Details": {
              "message": "Building Task Directory"
            }, 
            "TaskSignalReason": "", 
            "Type": "Task Setup", 
            "DiskLimit": 0, 
            "DriverError": "", 
            "KillReason": "", 
            "FailsTask": false, 
            "FailedSibling": "", 
            "ValidationError": "", 
            "Signal": 0, 
            "KillError": "", 
            "DownloadError": "", 
            "Time": 1594939878425720632, 
            "RestartReason": "", 
            "DriverMessage": "", 
            "StartDelay": 0, 
            "GenericSource": "", 
            "SetupError": "", 
            "Message": "Building Task Directory", 
            "ExitCode": 0
          }, 
          {
            "DisplayMessage": "3 errors occurred:\n\t* failed to parse config: \n\t* Invalid label: No argument or block type is named \"memory_hard_limit\".\n\t* Incorrect attribute value type: Inappropriate value for attribute \"args\": list of string required.\n\n", 
            "TaskSignal": "", 
            "KillTimeout": 0, 
            "VaultError": "", 
            "Details": {
              "validation_error": "3 errors occurred:\n\t* failed to parse config: \n\t* Invalid label: No argument or block type is named \"memory_hard_limit\".\n\t* Incorrect attribute value type: Inappropriate value for attribute \"args\": list of string required.\n\n"
            }, 
            "TaskSignalReason": "", 
            "Type": "Failed Validation", 
            "DiskLimit": 0, 
            "DriverError": "", 
            "KillReason": "", 
            "FailsTask": false, 
            "FailedSibling": "", 
            "ValidationError": "3 errors occurred:\n\t* failed to parse config: \n\t* Invalid label: No argument or block type is named \"memory_hard_limit\".\n\t* Incorrect attribute value type: Inappropriate value for attribute \"args\": list of string required.\n\n", 
            "Signal": 0, 
            "KillError": "", 
            "DownloadError": "", 
            "Time": 1594939878880141315, 
            "RestartReason": "", 
            "DriverMessage": "", 
            "StartDelay": 0, 
            "GenericSource": "", 
            "SetupError": "", 
            "Message": "", 
            "ExitCode": 0
          }, 
          {
            "DisplayMessage": "Error was unrecoverable", 
            "TaskSignal": "", 
            "KillTimeout": 0, 
            "VaultError": "", 
            "Details": {
              "fails_task": "true", 
              "restart_reason": "Error was unrecoverable"
            }, 
            "TaskSignalReason": "", 
            "Type": "Not Restarting", 
            "DiskLimit": 0, 
            "DriverError": "", 
            "KillReason": "", 
            "FailsTask": true, 
            "FailedSibling": "", 
            "ValidationError": "", 
            "Signal": 0, 
            "KillError": "", 
            "DownloadError": "", 
            "Time": 1594939878923985973, 
            "RestartReason": "Error was unrecoverable", 
            "DriverMessage": "", 
            "StartDelay": 0, 
            "GenericSource": "", 
            "SetupError": "", 
            "Message": "", 
            "ExitCode": 0
          }, 
          {
            "DisplayMessage": "Sent interrupt. Waiting 5s before force killing", 
            "TaskSignal": "", 
            "KillTimeout": 5000000000, 
            "VaultError": "", 
            "Details": {
              "kill_timeout": "5s"
            }, 
            "TaskSignalReason": "", 
            "Type": "Killing", 
            "DiskLimit": 0, 
            "DriverError": "", 
            "KillReason": "", 
            "FailsTask": false, 
            "FailedSibling": "", 
            "ValidationError": "", 
            "Signal": 0, 
            "KillError": "", 
            "DownloadError": "", 
            "Time": 1594939878991979772, 
            "RestartReason": "", 
            "DriverMessage": "", 
            "StartDelay": 0, 
            "GenericSource": "", 
            "SetupError": "", 
            "Message": "", 
            "ExitCode": 0
          }, 
          {
            "DisplayMessage": "Sent interrupt. Waiting 5s before force killing", 
            "TaskSignal": "", 
            "KillTimeout": 5000000000, 
            "VaultError": "", 
            "Details": {
              "kill_timeout": "5s"
            }, 
            "TaskSignalReason": "", 
            "Type": "Killing", 
            "DiskLimit": 0, 
            "DriverError": "", 
            "KillReason": "", 
            "FailsTask": false, 
            "FailedSibling": "", 
            "ValidationError": "", 
            "Signal": 0, 
            "KillError": "", 
            "DownloadError": "", 
            "Time": 1594939879211605021, 
            "RestartReason": "", 
            "DriverMessage": "", 
            "StartDelay": 0, 
            "GenericSource": "", 
            "SetupError": "", 
            "Message": "", 
            "ExitCode": 0
          }
        ]
      }
    }
  }
]"""

# get_jobs_list output
"""[
  {
    "ParameterizedJob": false, 
    "Status": "dead", 
    "Name": "test", 
    "JobModifyIndex": 215, 
    "Stop": false, 
    "ModifyIndex": 264, 
    "StatusDescription": "", 
    "Priority": 50, 
    "SubmitTime": 1594939878224512417, 
    "Periodic": false, 
    "JobSummary": {
      "Namespace": "default", 
      "ModifyIndex": 231, 
      "Summary": {
        "test": {
          "Complete": 0, 
          "Lost": 0, 
          "Failed": 2, 
          "Running": 0, 
          "Starting": 0, 
          "Queued": 0
        }
      }, 
      "CreateIndex": 215, 
      "Children": {
        "Running": 0, 
        "Dead": 0, 
        "Pending": 0
      }, 
      "JobID": "test"
    }, 
    "ParentID": "", 
    "Datacenters": [
      "dc1"
    ], 
    "CreateIndex": 215, 
    "Type": "batch", 
    "ID": "test"
  }, 
  {
    "ParameterizedJob": false, 
    "Status": "running", 
    "Name": "test2", 
    "JobModifyIndex": 265, 
    "Stop": false, 
    "ModifyIndex": 268, 
    "StatusDescription": "", 
    "Priority": 50, 
    "SubmitTime": 1594993183940262509, 
    "Periodic": false, 
    "JobSummary": {
      "Namespace": "default", 
      "ModifyIndex": 268, 
      "Summary": {
        "test": {
          "Complete": 0, 
          "Lost": 0, 
          "Failed": 0, 
          "Running": 0, 
          "Starting": 1, 
          "Queued": 0
        }
      }, 
      "CreateIndex": 265, 
      "Children": {
        "Running": 0, 
        "Dead": 0, 
        "Pending": 0
      }, 
      "JobID": "test2"
    }, 
    "ParentID": "", 
    "Datacenters": [
      "dc1"
    ], 
    "CreateIndex": 265, 
    "Type": "batch", 
    "ID": "test2"
  }, 
  {
    "ParameterizedJob": false, 
    "Status": "pending", 
    "Name": "test3", 
    "JobModifyIndex": 267, 
    "Stop": false, 
    "ModifyIndex": 267, 
    "StatusDescription": "", 
    "Priority": 50, 
    "SubmitTime": 1594993184050862011, 
    "Periodic": false, 
    "JobSummary": {
      "Namespace": "default", 
      "ModifyIndex": 267, 
      "Summary": {
        "test": {
          "Complete": 0, 
          "Lost": 0, 
          "Failed": 0, 
          "Running": 0, 
          "Starting": 0, 
          "Queued": 0
        }
      }, 
      "CreateIndex": 267, 
      "Children": {
        "Running": 0, 
        "Dead": 0, 
        "Pending": 0
      }, 
      "JobID": "test3"
    }, 
    "ParentID": "", 
    "Datacenters": [
      "dc1"
    ], 
    "CreateIndex": 267, 
    "Type": "batch", 
    "ID": "test3"
  }
]"""