from django.views.generic import DetailView, ListView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect, HttpResponseServerError

from ClusterGateway.cluster_manager.models import Task
from ClusterGateway.cluster_manager.forms import TaskForm


class TasksList(LoginRequiredMixin, ListView):
    login_url = "/login/"
    context_object_name = 'tasks'
    template_name = "tasks_table.html"

    def get_queryset(self):
        user = self.request.user
        return Task.objects.filter(user=user)


class TaskCreate(LoginRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = "task_create.html"
    form_class = TaskForm
    model = Task

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.user = self.request.user
        if obj.save():
            return HttpResponseRedirect("/tasks/")
        else:
            return HttpResponseServerError("error in creating job contact administrators(view help in dashboard)")


class TaskDetail(LoginRequiredMixin, DetailView):
    login_url = "/login/"
    template_name = "task_single.html"
    slug_field = "job_id"
    slug_url_kwarg = "job_id"
    queryset = Task.objects.all()
    model = Task

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['logs'] = self.object.get_logs()
        return context
