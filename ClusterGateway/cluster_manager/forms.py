import shlex
import re

from django.forms.models import ModelForm
from django.forms import forms

from ClusterGateway.cluster_manager.models import Task


class TaskForm(ModelForm):
    class Meta:
        model = Task
        exclude = ["user", "job_id", "status", "status_message"]

    env_var_regex = r'^([a-zA-Z0-9]+="[^"]+"\s)*([a-zA-Z0-9]+="[^"]+")?$'
    rgx = re.compile(env_var_regex)

    def clean_command(self):
        cmd = self.cleaned_data['command']
        if len(cmd.split()) != 1:
            raise forms.ValidationError("cmd must be the binary you want to execute")
        else:
            return cmd

    def clean_args(self):
        args = self.cleaned_data['args']
        try:
            shlex.split(args)
        except ValueError:
            raise forms.ValidationError("args is not a valid shell argument")
        else:
            return args

    def clean_env_vars(self):
        env_vars = self.cleaned_data['env_vars']
        if self.rgx.match(env_vars) is None:
            raise forms.ValidationError(f"env vars must match regex {self.env_var_regex}")
        else:
            return env_vars
